package com.aeroshi.enteprises.model.repository

import com.aeroshi.enteprises.model.NetworkAPI
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class EnterpriseRepository {

    fun doEnteprises(
        uid: String,
        client: String,
        accessToken: String
    ): Single<String> {
        return NetworkAPI.getGitService()
            .getEnterprises(uid, client, accessToken)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun doLogin(email: String, password: String): Single<retrofit2.Response<String>> {
        return NetworkAPI.getGitService()
            .login(email, password)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}