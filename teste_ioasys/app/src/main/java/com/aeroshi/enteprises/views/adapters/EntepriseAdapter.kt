package com.aeroshi.enteprises.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.aeroshi.enteprises.R
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.views.adapters.viewHolders.EntepriseViewHolder
import java.util.*
import kotlin.collections.ArrayList


class EntepriseAdapter(
    private val context: Context,
    private val mItemClickListener: ItemClickListener
) :
    RecyclerView.Adapter<EntepriseViewHolder>(), Filterable {


    private val inflater: LayoutInflater by lazy { LayoutInflater.from(context) }

    var enterpises: ArrayList<Enterprise> = arrayListOf()
    var baseEnterprises: ArrayList<Enterprise> = arrayListOf()

    interface ItemClickListener {
        fun onItemClick(rep: Enterprise)
    }


    override fun getItemCount(): Int = enterpises.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntepriseViewHolder =
        EntepriseViewHolder(inflater.inflate(R.layout.layout_list_enteprises, parent, false))

    override fun onBindViewHolder(holder: EntepriseViewHolder, position: Int) {
        val enterprise = enterpises[position]

        holder.bind(enterprise)

        holder.itemView.setOnClickListener {
            mItemClickListener.onItemClick(enterpises[position])
        }
    }

    fun update(enteprises: ArrayList<Enterprise>) {
        this.enterpises = enteprises
        this.baseEnterprises = enteprises
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {
        var enteprisesFilterList: ArrayList<Enterprise>
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                enteprisesFilterList = if (charSearch.isEmpty()) {
                    baseEnterprises
                } else {
                    val resultList = ArrayList<Enterprise>()
                    for (row in enterpises) {
                        if (row.enterpriseName.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = enteprisesFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                enterpises = results?.values as ArrayList<Enterprise>
                notifyDataSetChanged()
            }

        }
    }


}

