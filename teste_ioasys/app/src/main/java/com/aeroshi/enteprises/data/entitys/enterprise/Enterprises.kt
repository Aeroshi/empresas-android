package com.aeroshi.enteprises.data.entitys.enterprise


import com.google.gson.annotations.SerializedName

data class Enterprises(
    @SerializedName("enterprises")
    val enterprises: List<Enterprise>
)