package com.aeroshi.enteprises.util

import java.util.regex.Matcher
import java.util.regex.Pattern


class StringUtil {

    companion object {
        const val EMPTY = ""

         fun isValidEmail(email: String): Boolean {
            val EMAIL_PATTERN = ("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
            val pattern: Pattern = Pattern.compile(EMAIL_PATTERN)
            val matcher: Matcher = pattern.matcher(email)
            return matcher.matches()
        }

         fun isValidPassword(pass: String?): Boolean {
            return pass != null && pass.isNotEmpty()
        }
    }

}