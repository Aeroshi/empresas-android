package com.aeroshi.enteprises.data.entitys.user

class User(
    var accessToken: String? = "",
    var client: String? = "",
    var uid: String? = ""
)