package com.aeroshi.enteprises.viewmodels


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprises
import com.aeroshi.enteprises.data.entitys.user.User
import com.aeroshi.enteprises.extensions.logError
import com.aeroshi.enteprises.model.repository.EnterpriseRepository
import com.aeroshi.enteprises.util.BaseSchedulerProvider
import com.aeroshi.enteprises.util.SchedulerProvider
import com.aeroshi.enteprises.util.enuns.ErrorType
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy


class HomeViewModel(
    private val mRepository: EnterpriseRepository = EnterpriseRepository(),
    private val mScheduler: BaseSchedulerProvider = SchedulerProvider()
) : ViewModel() {
    companion object {
        private const val TAG = "HomeViewModel"
    }

    private val mCompositeDisposable = CompositeDisposable()

    val mEnterprises = MutableLiveData<ArrayList<Enterprise>>()
    val mError = MutableLiveData<ErrorType>()

    override fun onCleared() {
        super.onCleared()
        clearDisposables()
    }

    fun clearDisposables() = mCompositeDisposable.clear()


    fun doEnteprises(user: User) {
        mCompositeDisposable.add(
            mRepository
                .doEnteprises(user.uid.toString(),user.client.toString(),user.accessToken.toString())
                .subscribeOn(mScheduler.io())
                .observeOn(mScheduler.ui())
                .subscribeBy(
                    onSuccess = { jsonResult ->
                        try {
                            val enteprises = jsonParser(jsonResult)
                            setResult(enteprises)
                        } catch (exception: Exception) {
                            mError.value = ErrorType.PARSER
                            logError(TAG, "Error on parser enteprises", exception)
                        }
                    },
                    onError = {
                        mError.value = ErrorType.NETWORK
                        logError(TAG, "Error on get enteprises", it)
                    }
                )
        )
    }

    private fun jsonParser(json: String): ArrayList<Enterprise> =
        Gson().fromJson(json, Enterprises::class.java).enterprises as ArrayList

    private fun setResult(repositories: ArrayList<Enterprise>) {
        val tempList = mEnterprises.value ?: arrayListOf()
        tempList.addAll(repositories)
        mEnterprises.value = tempList
    }


}