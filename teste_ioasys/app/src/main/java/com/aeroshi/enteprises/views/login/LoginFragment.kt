package com.aeroshi.enteprises.views.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.aeroshi.enteprises.R
import com.aeroshi.enteprises.data.entitys.user.User
import com.aeroshi.enteprises.databinding.FragmentLoginBinding
import com.aeroshi.enteprises.extensions.navigate
import com.aeroshi.enteprises.util.StringUtil
import com.aeroshi.enteprises.util.enuns.ErrorType
import com.aeroshi.enteprises.viewmodels.LoginViewModel
import com.aeroshi.enteprises.viewmodels.MainViewModel
import com.aeroshi.enteprises.views.MainActivity
import com.google.android.material.snackbar.Snackbar

class LoginFragment : Fragment() {

    companion object {
        private const val TAG = "LoginFragment"
    }

    private lateinit var mMainActivity: MainActivity

    private lateinit var mMainViewModel: MainViewModel

    private lateinit var mViewModel: LoginViewModel

    private lateinit var mBinding: FragmentLoginBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mMainActivity = activity as MainActivity


        if (!isMainViewModelInitialized())
            initializeMainViewModel()

        if (!isViewModelInitialized())
            initializeViewModel()

        if (!isBindingInitialized())
            initializeBinding(inflater, container)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addViewModelObservers()
    }


    private fun isMainViewModelInitialized() = ::mMainViewModel.isInitialized

    private fun initializeMainViewModel() {
        mMainViewModel = ViewModelProvider(mMainActivity).get(MainViewModel::class.java)
    }

    private fun isViewModelInitialized() = ::mViewModel.isInitialized

    private fun initializeViewModel() {
        mViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
    }


    private fun isBindingInitialized() = ::mBinding.isInitialized

    private fun initializeBinding(inflater: LayoutInflater, container: ViewGroup?) {
        mBinding = FragmentLoginBinding.inflate(inflater, container, false)
        mBinding.lifecycleOwner = this
        setupClickEvents()
        mMainActivity.hideBar()
    }

    private fun addViewModelObservers() {
        mViewModel.mError.observe(viewLifecycleOwner, observeError())
        mViewModel.mUser.observe(viewLifecycleOwner, observeUser())
    }

    private fun observeError(): Observer<ErrorType> {
        return Observer {
            if (it != ErrorType.NONE) {
                mBinding.textViewInvalid.visibility = View.VISIBLE
                mBinding.progressBar.visibility = View.GONE
            }
        }
    }

    private fun observeUser(): Observer<User> {
        return Observer {
            mBinding.progressBar.visibility = View.GONE
            mMainViewModel.mLoggedUser.value = it
            mViewModel.mError.value = ErrorType.NONE
            navigate(R.id.navigate_login_to_home)
        }
    }


    private fun setupClickEvents() {
        mBinding.textViewInvalid.visibility = View.INVISIBLE
        mBinding.buttonLogin.setOnClickListener {

            if (StringUtil.isValidEmail(mBinding.editTextTextEmailAddress.text.toString())
                && StringUtil.isValidPassword(mBinding.editTextTextPassword.text.toString())
            ) {
                mBinding.progressBar.visibility = View.VISIBLE
                mViewModel.doLogin(
                    mBinding.editTextTextEmailAddress.text.toString(),
                    mBinding.editTextTextPassword.text.toString()
                )
            }
            if (!StringUtil.isValidEmail(mBinding.editTextTextEmailAddress.text.toString())) {
                mBinding.editTextTextEmailAddress.error =
                    mMainActivity.getString(R.string.imvalid_email)
            }
            if (!StringUtil.isValidPassword(mBinding.editTextTextEmailAddress.text.toString())) {
                mBinding.editTextTextPassword.error = mMainActivity.getString(R.string.empty_fields)
            }

        }
    }


}