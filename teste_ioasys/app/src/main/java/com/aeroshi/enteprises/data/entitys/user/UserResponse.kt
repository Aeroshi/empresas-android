package com.aeroshi.enteprises.data.entitys.user


import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("enterprise")
    val enterprise: Any?,
    @SerializedName("investor")
    val investor: Investor,
    @SerializedName("success")
    val success: Boolean
)