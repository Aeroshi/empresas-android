package com.aeroshi.enteprises.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.data.entitys.user.User

class MainViewModel : ViewModel() {
    val mSelectedEnterprise= MutableLiveData<Enterprise>()
    val mLoggedUser= MutableLiveData<User>()
}