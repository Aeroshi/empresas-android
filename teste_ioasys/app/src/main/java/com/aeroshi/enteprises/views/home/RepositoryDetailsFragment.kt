package com.aeroshi.enteprises.views.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.aeroshi.enteprises.R
import com.aeroshi.enteprises.databinding.FragmentEnteprisesDetailsBinding
import com.aeroshi.enteprises.extensions.navigate
import com.aeroshi.enteprises.util.Constants.Companion.BASE_URL
import com.aeroshi.enteprises.viewmodels.MainViewModel
import com.aeroshi.enteprises.views.MainActivity
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso

class RepositoryDetailsFragment : Fragment() {

    companion object {
        private const val TAG = "RepDetailsFragment"
    }

    private lateinit var mMainActivity: MainActivity

    private lateinit var mMainViewModel: MainViewModel

    private lateinit var mBinding: FragmentEnteprisesDetailsBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mMainActivity = activity as MainActivity


        if (!isMainViewModelInitialized())
            initializeMainViewModel()

        if (!isBindingInitialized())
            initializeBinding(inflater, container)

        return mBinding.root
    }


    private fun isMainViewModelInitialized() = ::mMainViewModel.isInitialized

    private fun initializeMainViewModel() {
        mMainViewModel = ViewModelProvider(mMainActivity).get(MainViewModel::class.java)
    }


    private fun isBindingInitialized() = ::mBinding.isInitialized

    private fun initializeBinding(inflater: LayoutInflater, container: ViewGroup?) {
        mBinding = FragmentEnteprisesDetailsBinding.inflate(inflater, container, false)
        mBinding.lifecycleOwner = this
        try {
            val imgUrl = "$BASE_URL${mMainViewModel.mSelectedEnterprise.value!!.photo}"
            Picasso.get()
                .load(imgUrl)
                .into(mBinding.imageViewUserName)
            mBinding.imageViewUserName.scaleType = ImageView.ScaleType.CENTER_CROP
            mBinding.textViewDesc.text = mMainViewModel.mSelectedEnterprise.value!!.description
            mMainActivity.changeTitleBar(mMainViewModel.mSelectedEnterprise.value!!.enterpriseName)
            mMainActivity.showBar()
        } catch (exception: Exception) {
            Snackbar.make(
                mBinding.principal,
                R.string.error_details_enterpise,
                Snackbar.LENGTH_LONG
            ).show()
            navigate(R.id.navigate_repositoryDetailsFragment_to_home)
            Log.e(TAG, "Error on set details of repository: ${exception.localizedMessage}")
            exception.stackTrace
        }
    }



}