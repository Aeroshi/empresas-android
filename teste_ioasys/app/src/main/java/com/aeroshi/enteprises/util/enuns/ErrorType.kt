package com.aeroshi.enteprises.util.enuns

enum class ErrorType {
    NONE,
    DATA,
    NETWORK,
    PARSER
}