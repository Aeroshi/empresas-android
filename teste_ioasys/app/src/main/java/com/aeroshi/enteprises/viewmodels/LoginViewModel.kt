package com.aeroshi.enteprises.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprises
import com.aeroshi.enteprises.data.entitys.user.User
import com.aeroshi.enteprises.data.entitys.user.UserResponse
import com.aeroshi.enteprises.extensions.logDebug
import com.aeroshi.enteprises.extensions.logError
import com.aeroshi.enteprises.extensions.logWarning
import com.aeroshi.enteprises.model.repository.EnterpriseRepository
import com.aeroshi.enteprises.util.BaseSchedulerProvider
import com.aeroshi.enteprises.util.SchedulerProvider
import com.aeroshi.enteprises.util.enuns.ErrorType
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy


class LoginViewModel(
    private val mRepository: EnterpriseRepository = EnterpriseRepository(),
    private val mScheduler: BaseSchedulerProvider = SchedulerProvider()
) : ViewModel() {
    companion object {
        private const val TAG = "LoginViewModel"
    }

    private val mCompositeDisposable = CompositeDisposable()

    val mUser = MutableLiveData<User>()
    val mError = MutableLiveData<ErrorType>()
    val mLoading = MutableLiveData<Boolean>()

    override fun onCleared() {
        super.onCleared()
        clearDisposables()
    }

    fun clearDisposables() = mCompositeDisposable.clear()


    fun doLogin(login: String, password: String) {
        mLoading.value = true
        mCompositeDisposable.add(
            mRepository
                .doLogin(login, password)
                .subscribeOn(mScheduler.io())
                .observeOn(mScheduler.ui())
                .subscribeBy(
                    onSuccess = { response ->
                        try {
                            mLoading.value = false
                            val result = jsonParser(response.body().toString())
                            if (result.success) {
                                val headers = response.headers()
                                mUser.value = User(
                                    headers.get("access-token"),
                                    headers.get("client"),
                                    headers.get("uid")
                                )
                            } else {
                                mError.value = ErrorType.DATA
                                logWarning(TAG, "Error on entered data")
                            }

                        } catch (exception: Exception) {
                            mError.value = ErrorType.PARSER
                            logError(TAG, "Error on set auth tokens", exception)
                            mLoading.value = false
                        }
                    },
                    onError = {
                        mLoading.value = false
                        mError.value = ErrorType.NETWORK
                        logError(TAG, "Error on login", it)
                    }
                ))

    }

    private fun jsonParser(json: String): UserResponse =
        Gson().fromJson(json, UserResponse::class.java)
}