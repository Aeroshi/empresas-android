package com.aeroshi.enteprises.views.adapters.viewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.aeroshi.enteprises.R
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.extensions.logError
import com.aeroshi.enteprises.util.Constants.Companion.BASE_URL
import com.squareup.picasso.Picasso
import java.lang.Exception

class EntepriseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val textViewTitle: TextView = view.findViewById(R.id.textViewTitle)
    private val textViewService: TextView = view.findViewById(R.id.textViewService)
    private val textViewCountry: TextView = view.findViewById(R.id.textViewCountry)
    private val imgView: ImageView = view.findViewById(R.id.imageView)

    fun bind(enterprise: Enterprise) {
        textViewTitle.text = enterprise.enterpriseName
        textViewService.text = enterprise.enterpriseType.enterpriseTypeName
        textViewCountry.text = enterprise.country
        try {
            val imgUrl = "$BASE_URL${enterprise.photo}"
            Picasso.get()
                .load(imgUrl)
                .into(imgView)
            imgView.scaleType = ImageView.ScaleType.CENTER_CROP
        }catch(ex: Exception){
            logError("EntepriseViewHolder","Error on set IMG",ex)
        }
    }

}