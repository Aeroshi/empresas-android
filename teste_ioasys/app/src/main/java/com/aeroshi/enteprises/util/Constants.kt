package com.aeroshi.enteprises.util

class Constants {

    companion object {
        const val BASE_URL = "https://empresas.ioasys.com.br"
        const val API_URL = "${BASE_URL}/api/v1/"
    }
}