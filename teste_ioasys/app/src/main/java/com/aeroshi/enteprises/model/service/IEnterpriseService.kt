package com.aeroshi.enteprises.model.service

import com.aeroshi.enteprises.data.entitys.user.User
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

interface IEnterpriseService {

    @GET("enterprises")
    fun getEnterprises(
        @Header("uid") uid: String,
        @Header("client") client: String,
        @Header("access-token") accessToken: String
    ): Single<String>

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Single<Response<String>>

}