package com.aeroshi.enteprises.views.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aeroshi.enteprises.R
import com.aeroshi.enteprises.data.entitys.enterprise.Enterprise
import com.aeroshi.enteprises.databinding.FragmentHomeBinding
import com.aeroshi.enteprises.extensions.logDebug
import com.aeroshi.enteprises.extensions.navigate
import com.aeroshi.enteprises.util.Executors.Companion.ioThread
import com.aeroshi.enteprises.util.InternetUtil.Companion.isOnline
import com.aeroshi.enteprises.util.enuns.ErrorType
import com.aeroshi.enteprises.viewmodels.HomeViewModel
import com.aeroshi.enteprises.viewmodels.MainViewModel
import com.aeroshi.enteprises.views.MainActivity
import com.aeroshi.enteprises.views.adapters.EntepriseAdapter
import com.google.android.material.snackbar.Snackbar


class HomeFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener,
    EntepriseAdapter.ItemClickListener {

    companion object {
        private const val TAG = "HomeFragment"
    }

    private lateinit var mMainActivity: MainActivity

    private lateinit var mMainViewModel: MainViewModel

    private lateinit var mViewModel: HomeViewModel

    private lateinit var mBinding: FragmentHomeBinding

    private lateinit var mAdapter: EntepriseAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mMainActivity = activity as MainActivity


        if (!isMainViewModelInitialized())
            initializeMainViewModel()

        if (!isViewModelInitialized())
            initializeViewModel()

        if (!isBindingInitialized())
            initializeBinding(inflater, container)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addViewModelObservers()
    }

    override fun onResume() {
        super.onResume()
        mBinding.search.setQuery("", false)
        mBinding.search.clearFocus()
        mMainActivity.hideBar()
    }

    override fun onRefresh() {
        if (isOnline(mMainActivity.applicationContext)) {
            mViewModel.mEnterprises.value?.clear()
            mAdapter.enterpises.clear()
            ioThread {
                mViewModel.doEnteprises(mMainViewModel.mLoggedUser.value!!)
            }
        } else {
            mBinding.swipeRefresh.isRefreshing = false
            Snackbar.make(
                mBinding.principal,
                R.string.error_no_connection,
                Snackbar.LENGTH_SHORT
            ).show()
        }

    }

    override fun onStop() {
        logDebug(TAG, "onStop")
        super.onStop()
        mViewModel.clearDisposables()
    }


    override fun onItemClick(rep: Enterprise) {
        mMainViewModel.mSelectedEnterprise.value = rep
        this.navigate(R.id.navigate_home_to_repositoryDetailsFragment)
    }


    private fun isMainViewModelInitialized() = ::mMainViewModel.isInitialized

    private fun initializeMainViewModel() {
        mMainViewModel = ViewModelProvider(mMainActivity).get(MainViewModel::class.java)
    }

    private fun isViewModelInitialized() = ::mViewModel.isInitialized

    private fun initializeViewModel() {
        mViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    private fun addViewModelObservers() {
        mViewModel.mError.observe(viewLifecycleOwner, observeError())
        mViewModel.mEnterprises.observe(viewLifecycleOwner, observeRepositories())
    }

    private fun isBindingInitialized() = ::mBinding.isInitialized

    private fun initializeBinding(inflater: LayoutInflater, container: ViewGroup?) {
        mBinding = FragmentHomeBinding.inflate(inflater, container, false)
        mBinding.lifecycleOwner = this
        mBinding.swipeRefresh.setOnRefreshListener(this)
        startLoading()
        setAdapter()
        ioThread {
            mViewModel.doEnteprises(mMainViewModel.mLoggedUser.value!!)
        }
        listeners()
        mMainActivity.hideBar()
    }

    private fun listeners() {

        mBinding.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mAdapter.filter.filter(newText)
                return false
            }

        })
    }


    private fun observeError(): Observer<ErrorType> {
        return Observer {
            finishLoading()
            if (it != ErrorType.NONE) {
                Snackbar.make(
                    mBinding.principal,
                    R.string.error_get_enterpise,
                    Snackbar.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun observeRepositories(): Observer<ArrayList<Enterprise>> {
        return Observer {
            mViewModel.mError.value = ErrorType.NONE
            if (it.isNotEmpty()) {
                finishLoading()
                mAdapter.update(it)
            }
        }
    }

    private fun finishLoading() {
        mBinding.swipeRefresh.isRefreshing = false
    }

    private fun startLoading() {
        mBinding.swipeRefresh.isRefreshing = true
    }

    private fun setAdapter() {
        mAdapter = EntepriseAdapter(mMainActivity.applicationContext, this)
        mBinding.recycleViewRepositories.layoutManager =
            LinearLayoutManager(mMainActivity.applicationContext)
        mBinding.recycleViewRepositories.adapter = mAdapter
    }

}