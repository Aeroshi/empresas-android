package com.aeroshi.enteprises.viewmodels

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.aeroshi.enteprises.data.entitys.user.User
import com.aeroshi.enteprises.model.repository.EnterpriseRepository
import com.aeroshi.enteprises.util.StringUtil
import com.aeroshi.enteprises.util.TrampolineSchedulerProvider
import com.aeroshi.enteprises.util.enuns.ErrorType
import io.reactivex.Single
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


@RunWith(JUnit4::class)
class HomeViewModelTest {

    companion object {
        private var mJsonSuccess: String? = null
        private var mJsonError: String? = null


        @BeforeClass
        @JvmStatic
        fun executeOnce() {
            this::class.java.classLoader?.let { classLoader ->
                mJsonSuccess = classLoader.getResource("json/successReturn.json").readText()
                mJsonError = classLoader.getResource("json/errorReturn.json").readText()
            }
        }
    }

    @get:Rule
    var mRule: TestRule = InstantTaskExecutorRule()

    private val context = Mockito.mock(Context::class.java)


    @Mock
    lateinit var enterpriseRepository: EnterpriseRepository



    private lateinit var mViewModel: HomeViewModel


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        mViewModel =
            HomeViewModel(
                enterpriseRepository,
                TrampolineSchedulerProvider()
            )
    }

    @Test
    fun HomeViewModel_doEnteprises_deve_retorna_sucesso() {
        // Preparing
        val fakeJsonReturn = mJsonSuccess
            ?: StringUtil.EMPTY

        // Mock
        Mockito.`when`(enterpriseRepository.doEnteprises("","",""))
            .thenReturn(Single.just(fakeJsonReturn))

        // Call
        mViewModel.doEnteprises(User("","",""))

        // Assert
        verify(enterpriseRepository, times(1)).doEnteprises("","","")

        Assert.assertNotNull(mViewModel.mEnterprises.value)
    }

    @Test
    fun HomeViewModel_doEnteprises_deve_retorna_erro_parser() {
        // Preparing
        val fakeJsonReturn = mJsonError
            ?: StringUtil.EMPTY

        // Mock
        Mockito.`when`(enterpriseRepository.doEnteprises("","",""))
            .thenReturn(Single.just(fakeJsonReturn))


        // Call
        mViewModel.doEnteprises(User("","",""))

        // Assert
        verify(enterpriseRepository, times(1)).doEnteprises("","","")

        Assert.assertEquals(ErrorType.PARSER, mViewModel.mError.value)
    }


}