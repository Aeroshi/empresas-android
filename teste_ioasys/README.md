### Como usar ###
* Abra o app e insira os dados de teste:
    * E-mail: testeapple@ioasys.com.br
    * Senha: 12341234
* Após o Login:
    * Para atualizar a lista deve-se fazer um gesto swipe na vertical.
    * Para visualizar os detalhes de uma empresa, pressione no item desejado.
    * Se desejar pesquisar uma empresa específica que já foi carregada, pode-se utilizar o campo de pesquisa.

### Bibliotecas usadas ###
* Retrofit para fazer as requisições. Foi usado pela sua simplicidade e ser muito eficaz.
* Picasso para exibir imagem da empresa. Foi usada por eu ter mais familiaridade com ela e a mesma  fazer o controle automático de cache e download de imagens.
* Navigator para o controle de transição de telas. Foi usada para ter de forma visual o mapa geral de transições de telas do aplicativo.
* Mockito e junit para os testes unitários. Por eu ter mais familiaridade e atender bem.

### Com mais tempo ###
* Deixaria o aplicativo 'offline first' para que não seja necessário fazer login e requisição das empresas toda a vez que abrir o mesmo.
* Trabalharia um pouco mais no design do aplicativo.
* Cobriria melhor com testes unitários.


